// app.js
/*
  day1 内容总结
  1. 注册小程序 - 一个邮箱只能注册一个小程序-拿到 appId
  2. 创建小程序 - 使用官方开发者工具 - 点击+号 - 创建
        1. 目录不要出现中文，最好是空目录
        2. 填入自己注册的 appId，不要选择云开发，不要选择模板
  3. 小程序适配：
    1. ✅✅rpx，把整个屏幕设置为 750rpx
    2. vw
  4. 小程序基本组件:
    1. view -- div
    2. text -- span
    3. swiper -- 轮播图
        swiper-item
    4. image -- img
    5. button
  5. 小程序数据和方法等怎么定义
    // 定数数据，直接写在data中
    // 需要定义方法，直接写即可
    // 读取通过 this.data.num
    // 修改数据，必须要通过 this.setData()
    ❌不能直接赋值
    data: { num:10 },
    add(){ }
  6. 全局配置(app.json),页面配置
        pages: 小程序的页面都要写在这里
        window： 小程序的窗口配置，标题文本，文本颜色，标题背景色等
        tabBar：小程序的底部导航,✨✨注意 图片只能使用 本地图片
        entryPagePath: 小程序的首页设置
  7. 小程序使用 字体图标 和背景图
    静态资源都建议使用 网络资源
    字体图标：跟网页一样  ，但是不支持本地图片
    1. @import '路径'
    2. <text class="iconfont .icon-shoucang red">收藏</text>

    背景图：跟网页一样 ，但是不支持本地图片

*/

/* 
day2 总结
1. 上线的注意点：
  小程序的主包体积（不能超过 2M), 1. 静态资源使用线上 2. 分包

2. 
 <input value="{{value}}" bindinput="add">  
   模板语法： 通过两个花括号，绑定值
   绑定事件： bind事件名，bindtap(注意不要写大写 bindTap)

3. 列表渲染: 
    wx:for，默认的下标 index，默认每一项 item
    wx:for-index="i",wx:for-item="t"
    wx:key
      1. 本身作为key：wx:key="*this"
      2. 复杂数据： wx:key="id"

 4. 条件渲染
    wx:if  wx:elif  wx:else
    hidden: 条件成立的时候 隐藏
  5. 生命周期
    1. 应用的生命周期
        onLaunch:  初始化
        onShow: 页面显示
        onHide: 页面隐藏
    2. 页面的生命周期
        ✅onLoad: 页面初始化
        ✅onShow: 页面显示
        onHide: 页面隐藏
        onReady: 页面渲染你完成
        onUnload： 页面卸载
  6. 页面跳转
    1. 普通页面跳转wx.navigateTo
    2. tabBar页面跳转wx.switchTab
*/

/* 
day3 总结
1. 内置api
  1. wx.request
  2. 用户交互:
      1. wx.showLoading: 提示用户加载中,不会自动关闭
         wx.hideLoading: 关闭提醒
        场景： 发请求、页面等待中的 loading
      2. wx.showToast: 用户提醒，默认会关闭
          场景： 需要提醒用户的地方，例如表单校验
      3. wx.showModal: 用户确认操作
          场景： 一般是删除二次确认
  3. 本地存储：语法与原生几乎相似，注意，一定要使用 Sync 的 同步操作
    并且对于复杂数据(例如对象)，也不需要JSON.stringify 转化
    存数据：wx.setStorageSync('obj', { name: 'zs' })
    读数据：wx.getStorageSync('obj')
    删除一项数据：wx.removeStorageSync('obj')
    删除所有数据：wx.clearStorageSync()

  4. 获取用户头像
    1. button 设置属性 opentype='chooseAvatar'，
              绑定方法 bindchooseavatar="getUserAvatar"
    2. 用户点击，触发方法 getUserAvatar，从参数e拿到用户头像临时地址即可
2. 小程序中使用 npm
    1. npm init -y（初始化，让小程序有package.json依赖文件）
    2. 正常安装 npm i dayjs
    3. 每一次安装新的依赖，都要在开发者工具 - 工具 -构建
    4. 页面正常引进和使用

3. 自定义组件 大部分和vue相同
  1. 新建组件
  2. 页面注册才能使用(app.json全局注册，index.json页面局部注册)
  
  3.1 组件的数据
    data:{} 组件自身的数据
    // 传递过来的数据，需要定义接收，写法和vue几乎相同
    properties: {
      isLogin: Boolean,
      obj: {
        type: Object,
      },
      tips: {
        type: String,
        value: '未登录默认值',
      },
    },
  3.2 父传子
  <comA tips="传递的tips" />

  3.3 子传父
  this.triggerEvent('test','传递的数据')
  <comA tips="传递的tips" bingtest="父组件的方法" />

  3.4 插槽：传递了什么内容，就渲染什么内容
    1. 默认插槽，在组件内留下 <slot></slot> 即可
    2. 多插槽(具名插槽)，开启多插槽属性
      Component({
        options: {multipleSlots: true}
      })
      组件内留下 具名插槽
      <view>
        <slot name="header"></slot>
        <slot name="main"></slot>
        <slot name="footer"></slot>
      </view>

      页面使用
      <comA>
        <view slot="header">header</view>
        <view slot="main">main</view>
        <view slot="footer">footer</view>
      </comA>

  3.4 组件的生命周期和方法
  lifeTimes:{
    created(){},  使用较少，因为不能使用 this.setData({})
    attached(){}  使用较多
  },
  methods:{
    // 组件的方法需要都写在这里
  }

  4. 小程序使用 vant组件
  安装依赖 - 构建 npm - 全局(app.json)或单页面(index.json)引入-页面引进使用
  额外注意： 删除 app.json 中的 style:v2 字段

*/
App({
  /* 
    定义；在 App 中的数据和方法，都是全局的，页面通过 getApp() 可以获取到全局的实例
    场景：存放token
    扩展：项目中可能会使用 globalData 作为全局数据
  */
  token: '123',
  globalData: {
    id: 'id',
  },
  getToken() {
    console.log('我是app的getToken');
    return this.token;
  },
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function (params) {
    // console.log('onLaunch-小程序初始化', params);
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (params) {
    // console.log('onShow-看到小程序', params);
  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {
    // console.log('onHide-看不到小程序');
  },
});
