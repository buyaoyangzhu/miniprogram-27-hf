// components/comA/comA.js
/* 
1. 新建components文件夹
2. 新建组件的文件夹 comA
3. 开发者工具中右键comA，新建 Component
4. 全局注册(app.json) 或者 页面注册(index.json)
    ✨✨路径不用写 /miniprogram
    "usingComponents": {
     "comA": "/components/comA/comA"
    },
5. 页面使用
    <comA></comA>
*/
Component({
  // vue2接收
  // props:{
  //   tips: {
  //     type: String,
  //     default: '默认值'
  //   }
  // }
  /**
   * 组件的属性列表
   */
  properties: {
    tips: {
      type: String,
      value: '默认值',
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    msg: '我是组件的数据',
  },

  /**
   * 组件的方法列表
   */
  methods: {},
});
