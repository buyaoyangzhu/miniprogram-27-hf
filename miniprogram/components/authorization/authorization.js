// components/authorization/authorization.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isLogin: Boolean,
    obj: {
      type: Object,
    },
    tips: {
      type: String,
      value: '未登录默认值',
    },
  },

  /**
   * 组件的初始数据
   */
  data: {},

  /**
   * 组件的方法列表
   */
  methods: {
    onTap() {
      this.triggerEvent('changetips', '我改变了父组件的tips');
    },
  },

  lifetimes:{
    // 组件刚好创建完成，类似vue中的 created,使用较少
    created(){
      // 不能在这里调用 this.setDate({})
      // 一般给组件实例对象 添加一点页面用不到的属性
      this.id = 123
    },
    // 组件初始化完成，类似vue中的 mounted，这里就没有使用限制了
    attached(){
      // this.setData({})
      // 发请求等等
    }
  }
});
