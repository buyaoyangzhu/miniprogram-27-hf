// pages/index2/index2.js
Page({
  /* 
作业：
input输入框，绑定数据，监听输入事件，内容改变时同步到 value 中
注意标签要保证规范，一定要闭合

button，绑定tap事件，把 input 的内容 添加到 数组arr中

页面对数组渲染

*/
  /**
   * 页面的初始数据
   */
  data: {
    value: '我是输入框',
    arr: [],
    obj: { name: 'zs' },
    list: [
      {
        id: 1,
        name: 'zs',
        age: 20,
      },
      {
        id: 2,
        name: 'ls',
        age: 21,
      },
      {
        id: 3,
        name: 'ww',
        age: 22,
      },
    ],
  },

  onInput(e) {
    console.log('onInput', e.detail.value);
    this.setData({ value: e.detail.value });
  },

  onTap() {
    // 快速复制数组
    const newArr = [...this.data.arr];
    newArr.push(this.data.value);
    this.setData({ arr: newArr });
  },
});
