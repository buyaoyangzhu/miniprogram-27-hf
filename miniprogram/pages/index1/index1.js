// pages/index1/index1.js
Page({
  /**
   * 页面的初始数据
   */
  // data(){
  //   return{
  //     num:1
  //   }
  // }
  data: {
    num: 100,
  },

  // 定数数据，直接写在data中
  // 需要定义方法，直接写即可
  // 读取通过 this.data.num
  // 修改数据，必须要通过 this.setData()
  add() {
    console.log('add', this.data.num);
    // ❌不能直接赋值
    // ❌this.data.num = 101;
    // ❌this.data.num++;
    // ❌this.data.num += 101;
    this.setData({ num: this.data.num + 1 });
  },
  dec() {
    this.setData({ num: this.data.num - 1 });
  },
});
