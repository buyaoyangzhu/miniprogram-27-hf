// pages/index6/index6.js
/* 
传统网页  和 小程序 发请求的区别
1. 传统网页发请求， 底层ajax，axios 的底层也是ajax，所以传统网页可以正常使用
2. 小程序运行 在微信app 中，不是传统网页，底层也不是 ajax，小程序里面，不能使用 axios 发请求

wx.request  做统一的发送请求

网页端向服务器发请求，会跨域：1.协议 2.域名 3.端口
微信app服务器向后台服务器发请求，不会跨域
*/
Page({
  onLoad() {
    /* 
    发请求注意
    1. 需要在微信公众平台配置合法域名才能使用，在上线必须要配置才能使用
    2. 开发者工具 - 详情 - 本地设置 - 勾选不校验合法域名
    */
    wx.request({
      // 请求路径
      url: 'https://live-api.itheima.net/announcement',
      // axios中，get params , 其他方式data
      // 小程序中，传递参数，所有方式都使用 data
      // data: {
      //   id: '123',
      // },
      // 不要加s,写成 methods
      // method: 'GET',
      // 不要加s,写成 headers
      // header: { token: '123' },
      // success: (res) => {
      //   console.log('请求成功回调 -----> ', res);
      // },
      // fail: (res) => {
      //   console.log('请求失败回调 -----> ', res);
      // },
      // complete: (res) => {
      //   console.log('请求完成回调 -----> ', res);
      // },
    });
  },
});
