// pages/index8/index8.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '',
  },
  getUserAvatar(e) {
    console.log('获取用户头像', e.detail.avatarUrl);
    this.setData({ avatarUrl: e.detail.avatarUrl });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    /* 
      微信本地存储，语法与原生几乎相似，注意，一定要使用 Sync 的 同步操作
      语法和原生 localStorage 几乎相似
      注意的是，原生存 复杂类型数据，需要先转字符串
      localStorage.setItem("obj",JSON.stringify({name:'zs'}))
      原生读取复杂数据后，需要转会对象
      JSON.parse(localStorage.getItem('obj'))
    */
    // 工作中一般不使用 wx.setStorage 异步存储
    // wx.setStorageSync('test', 123);
    // ✨✨微信本地存储，不需要对复杂数据做任何处理
    // wx.setStorageSync('obj', { name: 'zs' });
    // const obj = wx.getStorageSync('obj');
    // console.log('obj -----> ', obj);
    // wx.removeStorageSync('obj');
    // wx.clearStorageSync();
  },
});
