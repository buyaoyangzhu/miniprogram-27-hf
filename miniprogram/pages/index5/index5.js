// pages/index5/index5.js
Page({
  goIndex4() {
    wx.navigateTo({ url: '/pages/index4/index4' });
  },

  goIndex() {
    wx.switchTab({ url: '/pages/index/index' });
  },

  /* 
  生命周期应用场景和常见错误
  常用：
  onLoad：发请求，获取页面参数
  onShow：发请求，获取小程序场景值等

  注意
  1. 生命周期是小驼峰命名法 onLoad，不要写成 onload
  2. 获取页面传递过来的参数，只能在 onLoad 中获取
  */
  /**
   * 生命周期函数--监听页面加载 - 类似vue2 - created
   * 场景： 发请求，获取页面参数
   */
  onLoad(options) {
    console.log('onLoad - 页面加载', options.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   * 场景：一般不怎么使用，添加 loading结束的动画效果
   */
  onReady() {
    // console.log('onReady - 页面渲染完成，类似 mounted');
  },

  /**
   * 生命周期函数--监听页面显示
   * 场景：也会发请求，获取小程序场景值等
   */
  onShow() {
    console.log('onShow - 监听页面显示');
  },

  /**
   * 生命周期函数--监听页面隐藏
   * 如果页面记录还在，那么进入的是onHide，页面其实没有被销毁
   * 重新进入，只会触发 onShow，不会触发onLoad
   */
  onHide() {
    console.log('onHide - 监听页面隐藏');
  },

  /**
   * 生命周期函数--监听页面卸载
   * 如果页面记录不在，那么进入的是onUnload，页面已经被销毁
   * 重新进入会触发 所有的生命周期
   */
  onUnload() {
    console.log('onUnload - 监听页面卸载');
  },
});
