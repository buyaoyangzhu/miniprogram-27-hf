// index.js
/* 
面试题：传统的web应用 和 小程序 构成有什么不同
传统的web： 三层结构 html(必须有) + css + js（结构+样式+逻辑）
小程序： 四层结构，虽然名字不同，但是功能是相似的
wxml + js + wxss + json(配置)
*/

/* 
传统网页适配:
1. vw 把屏幕分成100等份,1vw等于屏幕宽度的1%
2. rem+flexible, 1rem = html中font-size的大小

小程序适配：
1. vw
2. ✅✅rpx，所有的屏幕，宽度一律都是750rpx

iphone6: 375px = 750rpx  ==>   1px=2rpx
phone: 750px = 750rpx  ==>   1px=1rpx
phone2: 411px/411 = 750rpx/411  ==>   1px=1.824rpx
*/
Page({
  onLoad() {
    const pages = getCurrentPages();
    console.log('首页pages -----> ', pages);
    const current = pages[pages.length - 1];
    console.log('首页current -----> ', current);
  },
});
