// pages/index10/index10.js
Page({
  onLoad() {
    // const app = getApp();
    // console.log('app -----> ', app);
    // console.log('app.token -----> ', app.token);
    // console.log('app.getToken() -----> ', app.getToken());

    /* 
      getCurrentPages() 获取页面的访问记录
      最后一个元素为当前页面
      注意点： switchTab, reLaunch 等方法，会清除普通页面记录 
     */
    const pages = getCurrentPages();
    console.log('pages -----> ', pages);
    const current = pages[pages.length - 1];
    console.log('current -----> ', current);
  },
});
