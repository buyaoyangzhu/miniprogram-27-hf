// pages/index7/index7.js
/* 
1. wx.showLoading: 提示用户加载中不会自动关闭
   wx.hideLoading: 关闭提醒
   场景： 发请求、页面等待中的 loading

2. wx.showToast: 用户提醒，默认会关闭
    场景： 需要提醒用户的地方，例如表单校验

3. wx.showModal: 用户确认操作
    场景： 一般是删除二次确认
*/
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // wx.showToast({ title: '成功', icon: 'success' });
    // wx.showToast({ title: '失败', icon: 'error' });
    // wx.showToast({ title: '等待', icon: 'loading' });
    // wx.showToast({ title: '无图标', icon: 'none' });

    wx.showModal({
      title: '删除',
      content: '您确定要逃课吗?',
      // 只要是用户点击，无论确定还是取消，都会进入 success 方法
      // 使用 res.confirm 判断是否是点击了确定
      success: (res) => {
        // console.log('res -----> ', res);
        if (res.confirm) {
          wx.showToast({ title: '点击了确定', icon: 'success' });
        } else {
          wx.showToast({ title: '点击了取消', icon: 'error' });
        }
      },
    });

    // wx.showLoading({ title: '加载中...' });
    wx.request({
      url: 'https://live-api.itheima.net/announcement',
      success: (res) => {
        console.log('res -----> ', res.data.data);
        this.setData({ list: res.data.data });
        // wx.hideLoading();
      },
      // success(){
      // 谁调用，this就指向谁
      // 箭头函数，定义时this指向哪里，我就是哪里，跟什么时候调用无关
      // }
    });
  },
});
