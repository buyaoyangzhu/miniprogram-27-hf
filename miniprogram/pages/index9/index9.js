// pages/index9/index9.js
import dayjs from 'dayjs';
Page({
  onLoad() {
    console.log(dayjs().format('DD/MM/YYYY'));
  },
  data: {
    isLogin: false,
    obj: { name: 'zs', age: 20 },
    tips: '暂未登录',
  },
  changetips(e) {
    console.log('父组件的changetips', e.detail);
    this.setData({ tips: e.detail });
  },
});

/* 
小程序使用npm
1. npm init -y 初始化项目，让小程序有 package.json 依赖文件
注意：目录不要出现中路，否则生成不了

2. npm i dayjs 安装要用的插件

3. 点击工具 - 构建 把依赖包需要用到的文件提取出来(miniprogram_npm)

4. 页面正常使用
    import dayjs from 'dayjs';
    console.log(dayjs().format('DD/MM/YYYY'));
*/
